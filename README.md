## Лабораторные работы по дисциплине "Организация ЭВМ и ассемблер"

#### Доступ к корневой папке лаб с отчётами по [ассемблеру](https://drive.google.com/drive/folders/1buF6CDKbKAdLOnKXrasD7wxWjbw4s_hD?usp=sharing).

___

**Отчеты на гугл документы:**

1.  Лабораторная работа 1

    [Представление данных в ЭВМ](https://docs.google.com/document/d/18aKrRlNMFCKtOodr1DpnLosdfgrc7BjQXvLkJ3j7cl8/edit?usp=sharing)


2.  Лабораторная работа 2

    [Представление данных в ЭВМ. Сравнение платформ](https://docs.google.com/document/d/1KAOnpkClPfRbkNUsJlUboZc2Ia1L7UW4CFCBhvMWA44/edit?usp=sharing)


3.  Лабораторная работа 3

    [Целочисленная арифметика и арифметика с плавающей запятой](https://docs.google.com/document/d/1gfSJ3Q7NhffaekFbkyAE2fp4rTDYeQKGaVHfyILNdIE/edit?usp=sharing)


4.  Лабораторная работа 4

    [Ввод-вывод при помощи libc](https://docs.google.com/document/d/1Izxg576Nxf7DG688BI4iT6cWAKrY6vzhaHDsoUbFfO8/edit?usp=sharing)


5.  Лабораторная работа 5

    [Использование ассемблерных вставок в программах на C++. Передача параметров во вставку. Перезаписываемые элементы](https://docs.google.com/document/d/1E_635fgfL5SE8EsylgQ8YFkEpViSy13LVh7wXy-Muzw/edit?usp=sharing)


6.  Лабораторная работа 6

     [Ассемблерные вставки в программах на C++. Целочисленная арифметика и арифметика с плавающей запятой](https://docs.google.com/document/d/1_Bh3C8bd87Kx9Byvr3vWWEHNWZ3wm-4Cutv0KcbNGR4/edit?usp=sharing)


7.  Лабораторная работа 7

    [Ассемблерные вставки в программах на C++. Флаги и условия](https://docs.google.com/document/d/1oEyl1R_HukLyyluzMeZ-29hFbjZzPGgQX-51jmAH0sk/edit?usp=sharing)


8.  Лабораторная работа 8

    [Модули и функции. Вызов функций libc и libm.](https://docs.google.com/document/d/1sPuFmtTJIsoRVBlVA8twxhILv84dj9ASxo9LTZ1mo2k/edit?usp=sharing)
